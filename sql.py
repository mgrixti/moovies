import MySQLdb

mydb = MySQLdb.connect( host = '10.0.130.1', user = 'micha00', passwd = '5StarHotel', db = 'movie_db')
cursor = mydb.cursor()



##SEARCHES


## List roles in movies an actor has played
## Input: actor.first_name, actor.last_name
## TESTED AND WORKING
def searchActor ( first_name, last_name ):
  var = [first_name, last_name]
  cursor.execute('SELECT first_name, last_name, movie.movie_id, title, role FROM role JOIN (actor, movie) ON (actor.actor_id=role.actor_id AND movie.movie_id=role.movie_id) WHERE first_name LIKE "%s" AND last_name LIKE "%s"', ('%' + var[0] + '%', '%' + var[1] + '%'))
  return cursor.fetchall()

## List actors with query string in their name
## Input: string in either actor.first_name or actor.last_name
## TESTED AND WORKING
def searchActorName ( search_name ):
  var = [ search_name ]
  cursor.execute('SELECT actor.actor_id, first_name, last_name FROM actor WHERE first_name LIKE ' +  '"%' + search_name + '%"' + 'OR last_name LIKE ' + '"%' + search_name + '%"')
  return cursor.fetchall()

## Return actors based on full name
## Input: actor.first_name, actor.last_name
## TESTED AND WORKING
def searchActorNameFull ( first, last ):
  cursor.execute('SELECT actor.actor_id, first_name, last_name FROM actor WHERE first_name LIKE ' +  '"%' + first + '%"' + 'AND last_name LIKE ' + '"%' + last + '%"')
  return cursor.fetchall()

## Return all roles and movies for an actor based on actor id
## Input: actor.actor_id
## TESTED AND WORKING
def searchActorRole ( a_id ):
  cursor.execute('SELECT role, title, movie.movie_id FROM role JOIN (actor, movie) ON (actor.actor_id=role.actor_id AND movie.movie_id=role.movie_id) WHERE role.actor_id="' + str(a_id) + '"')
  return cursor.fetchall()

def searchActorName ( a_id ):
  cursor.execute('SELECT first_name, last_name FROM actor WHERE actor_id="' + str(a_id) + '"')
  return cursor.fetchall()
  
## List movies a searched director has worked on
## Input: director.first_name, director.last_name
## TESTED AND WORKING
def searchDirector ( first_name, last_name ):
  var = [first_name, last_name]
  cursor.execute('SELECT director.director_id, first_name, last_name, movie.movie_id, title FROM directs_movie JOIN (director, movie) ON (director.director_id=directs_movie.director_id AND movie.movie_id=directs_movie.movie_id) WHERE first_name LIKE "%s" AND last_name LIKE "%s"', ('%' + var[0] + '%', '%' + var[1] + '%'))
  return cursor.fetchall()

## Search movies a director has worked on
## Input: director.director_id
def searchDirectorMovies ( d_id ):
  cursor.execute('SELECT title, movie.movie_id FROM directs_movie JOIN (director, movie) ON (director.director_id=directs_movie.director_id AND movie.movie_id=directs_movie.movie_id) WHERE directs_movie.director_id="' + 
  str(d_id) + '"')
  return cursor.fetchall()

## List directors with query string in their name
## Input: string in either director.first_name or director.last_name
## TESTED AND WORKING
def searchDirectorName ( search_name ):
  cursor.execute('SELECT director.director_id,  first_name, last_name FROM director WHERE first_name LIKE ' +  '"%' + search_name + '%"' + 'OR last_name LIKE ' + '"%' + search_name + '%"')
  return cursor.fetchall()

## Return directors based on full name
## Input: director.first_name, director.last_name
## TESTED AND WORKING
def searchDirectorNameFull ( first, last ):
  cursor.execute('SELECT director.director_id, first_name, last_name FROM director WHERE first_name LIKE ' +  '"%' + first + '%"' + 'AND last_name LIKE ' + '"%' + last + '%"')
  return cursor.fetchall()

## Return a directors name based on director ID
## Input: director.director_id
def searchDirectorNameID ( d_id ): 
  cursor.execute('SELECT first_name, last_name FROM director WHERE director_id="' + str(d_id) + '"')
  result = cursor.fetchone()
  first = result[0]
  last = result[1]
  final = [first, last]
  return final

## List movies based on a searched genre
## Input: genre.genre
## TESTED AND WORKING
def searchGenre ( genre_name ): 
  cursor.execute('SELECT genre.genre_id, genre, movie.movie_id,title FROM movie_genre JOIN (genre, movie) ON (genre.genre_id=movie_genre.genre_id AND movie.movie_id=movie_genre.movie_id) WHERE genre LIKE' + '"%' + genre_name + '%"')
  return cursor.fetchall()

## Find actors in a film based on movie title
## Input: movie.title
## TESTED AND WORKING
def searchMovie ( movie_title ):
  cursor.execute('SELECT movie.movie_id, title, first_name, last_name FROM role JOIN (actor, movie) ON (actor.actor_id=role.actor_id AND  movie.movie_id=role.movie_id) WHERE title LIKE' + '"%' + movie_title + '%"')
  return cursor.fetchall()

## Based on movie id return all actors and their roles in movie
## Input: movie.movie_id
## TESTED AND WORKING
def searchMovieActors ( m_id ):
  cursor.execute('SELECT actor.actor_id, first_name, last_name, role FROM role JOIN (actor) ON (actor.actor_id=role.actor_id) WHERE movie_id=' + '"' + str(m_id) + '"')
  return cursor.fetchall()

## Based on movie id return directors
## Input: movie.movie_id
## TESTED AND WORKING
def searchMovieDirector ( m_id ):
  cursor.execute('SELECT director.director_id, first_name, last_name FROM directs_movie JOIN (director) ON (director.director_id=directs_movie.director_id) WHERE movie_id=' + '"' + str(m_id) + '"')
  return cursor.fetchall()

## Based on movie id return genre
## Input: movie.movie_id
## TESTED AND WORKING
def searchMovieGenre ( m_id ):
  cursor.execute('SELECT genre FROM movie_genre JOIN (genre) ON (genre.genre_id=movie_genre.genre_id) WHERE movie_id=' + '"' + m_id + '"')
  return cursor.fetchall()

## Return all info on a specified movie
## Input: movie.movie_id
## TESTED AND WORKING
def searchMovieID( m_id ):
    cursor.execute('SELECT * FROM movie WHERE movie_id=' + '"' + m_id + '"')
    return cursor.fetchall()

## Find movies based on search query
## Input: movie.title
## TESTED AND WORKING
def searchMovieTitle( movie_title ):
  cursor.execute('SELECT * FROM movie WHERE title LIKE "%' + movie_title + '%"')
  return cursor.fetchall()




## ADD/REMOVE 


## Add director
## Do not call method directly, only call via updateMovieDirector
## Input: director.first_name, director.last_name
## TESTED AND WORKING
def addDirector ( first, last ):
  cursor.execute('INSERT INTO director (first_name, last_name) VALUES ("' + first + '", "' + last + '")' )
  mydb.commit()
  cursor.execute('SELECT director_id FROM director WHERE first_name="' + first + '"AND last_name="' + last + '"')
  d_id = cursor.fetchone()
  d_id = d_id[0]
  return d_id

## Add movie
## Input: movie.title
## TESTED AND WORKING
def addMovie ( newTitle ):
  cursor.execute('INSERT INTO movie (title) VALUES (' + '"'+ newTitle + '")')
  mydb.commit()
  cursor.execute('SELECT movie_id FROM movie WHERE title="' + newTitle + '"')
  m_id = cursor.fetchone()  
  m_id = m_id[0]
  return m_id

## Add row to movie_genre
## Input: genre.genre, movie.movie_id
## TESTED AND WORKING
def addMovieGenre ( givenGenre, m_id ):
  # Determines genre_id based on provided genre
  cursor.execute('SELECT genre_id FROM genre WHERE genre LIKE' +  '"%' + givenGenre + '%"')
  g_id = cursor.fetchone()
  g_id = g_id[0]
  if g_id != None:
    # Determine if movie/genre pair exists in movie_genre table yet
    cursor.execute('SELECT * FROM movie_genre WHERE movie_id="' + str(m_id) + '" AND genre_id="' + str(g_id) + '"')
    rows = cursor.fetchone()
    if rows == None: # Add a new row to movie_genre
      cursor.execute('INSERT INTO movie_genre (movie_id, genre_id) VALUES ("' + str(m_id) + '", "' + str(g_id) + '")')
      mydb.commit()
  else:
    # Add genre to database
    cursor.execute('INSERT INTO genre (genre) VALUES ("' + str(givenGenre) + '")')
    mydb.commit()
    # Retrieve new genre id
    cursor.execute('SELECT genre_id FROM genre WHERE genre="' + str(givenGenre) + '"')
    g_id = cursor.fetchone()
    g_id = g_id[0]
    # Add row to movie genre
    cursor.execute('INSERT INTO movie_genre (movie_id, genre_id) VALUES ("' + str(m_id) + '", "' + str(g_id) + '")')
    mydb.commit()

## Add row to directs_movie
## Do not call method directly, only call via updateMovieDirector
## Input: director.director_id, movie.movie_id
## TESTED AND WORKING
def addMovieDirector ( d_id, m_id ):
  cursor.execute('INSERT INTO directs_movie (director_id, movie_id) VALUES (' + str(d_id) + ', ' + str(m_id) + ')')
  mydb.commit()

## Remove row from movie_genre
## Input: genre.genre, movie.movie_id
## TESTED AND WORKING
def removeGenre( givenGenre, m_id ):
  # Determines genre_id based on provided genre
  cursor.execute('SELECT genre_id FROM genre WHERE genre LIKE' +  '"%' + givenGenre + '%"')
  g_id = cursor.fetchone()
  g_id = g_id[0]
  if g_id != None:
    # Determine if movie/genre pair exists in movie_genre table yet
    cursor.execute('SELECT * FROM movie_genre WHERE movie_id="' + str(m_id) + '" AND genre_id="' + str(g_id) + '"')
    rows = cursor.fetchone()
    if rows != None: # Remove row from movie_genre
      cursor.execute('DELETE FROM movie_genre WHERE genre_id="' + str(g_id) + '" AND movie_id="' + str(m_id) + '"')
      mydb.commit()

## Completely removes a movie from a database based on movie id
## Input: movie.movie_id
## TESTED AND WORKING
def removeMovie ( m_id ):
  # Delete from movie table
  cursor.execute('DELETE FROM movie WHERE movie_id="'+ str(m_id) + '"')
  mydb.commit()
  #  Delete from role table
  cursor.execute('DELETE FROM role WHERE movie_id="'+ str(m_id) + '"')
  mydb.commit()
  # Delete from movie_genre table
  cursor.execute('DELETE FROM movie_genre WHERE movie_id="' + str(m_id) + '"')
  mydb.commit()
  # Delete from directs_movie table
  cursor.execute('DELETE FROM directs_movie WHERE movie_id="'+ str(m_id) + '"')
  mydb.commit()
  complete = ('Movie ' + str(m_id) + ' has been removed from the database.')
  print (complete)



  
##UPDATE METHODS


## Update actor name
## Input: actor.first_name(new), actor.last_name(new), actor.actor_id
## TESTED AND WORKING
def updateActorName ( new_first, new_last, a_id ):
  cursor.execute('UPDATE actor SET first_name="' + new_first + '", last_name="' + new_last + '" WHERE actor_id="' + str(a_id) + '"')
  mydb.commit()
  
## Update director name
## Input: director.first_name(new), director.last_name(new), director.director_id
## TESTED AND WORKING
def updateDirectorName ( new_first, new_last, d_id ):
  cursor.execute('UPDATE director SET first_name="' + new_first + '", last_name="' + new_last + '" WHERE director_id="' + str(d_id) + '"')
  mydb.commit()
  
## Update movie's director based on director name and movie id
## Input: director.first_name, director.last_name, movie.movie_id
## TESTED AND WORKING
def updateMovieDirector ( givenFirst, givenLast, m_id ):
  # Find director in director table
  cursor.execute('SELECT director_id FROM director WHERE first_name="' + givenFirst + '" AND last_name="' + givenLast + '"')
  d_id = cursor.fetchone()
  d_id = d_id[0]
  # If director does not exist add them to director table
  if d_id == None:
    d_id = addDirector( givenFirst, givenLast )
  # Determine if movie exists in directs_movie yet
  cursor.execute('SELECT dir_movie_id FROM directs_movie WHERE movie_id="' + str(m_id) + '"' )
  dm_id = cursor.fetchone()
  dm_id = dm_id[0]
  if dm_id == None:
    addMovieDirector( d_id, m_id )
  else:
    cursor.execute('UPDATE directs_movie SET director_id="' + str(d_id) + '" WHERE movie_id="' + str(m_id) + '"')
    mydb.commit()

## Update title
## Input: movie.title(new), movie.movie_id
## TESTED AND WORKING
def updateTitle ( newTitle, m_id ):
  cursor.execute('UPDATE movie SET title= "'+ newTitle + '" WHERE movie_id="' + m_id + '"')
  mydb.commit()
