from flask import Flask
from flask import request
from flask import render_template
from flask import abort, redirect, url_for
from flask import session, escape
from flask.ext.login import LoginManager
from flask.ext.login import login_user, logout_user, login_required, current_user
import MySQLdb as mysql
import sql
from user import User
import ombdreq
import re

app = Flask(__name__, static_folder='static', static_url_path='')
mydb = mysql.connect(host='10.0.130.1', user='micha00', passwd='5StarHotel', db='movie_db')
cursor = mydb.cursor()
login_manager = LoginManager()
login_manager.init_app(app)
app.secret_key = '1q2w3e4r5t6y7u8i9o0p1234567890'


# Search for movies based on movie title. List multiple results
@app.route('/search')
@login_required
def search_movie():
    s_type = request.args.get('search_type', 'Example')
    movies = []
    if (s_type == "movie"):
        title = request.args.get('movie_title', 'Example')
        if (title == ""):
            return render_template('search.html')
        movies = sql.searchMovieTitle(title)
   
        return render_template('search.html', movies=movies, search_type=s_type)
    elif (s_type == "actor"):
        a_name = request.args.get('actor_name','Example')
        print a_name
        if (' ' in a_name):
            act_name = a_name.split(" ")
            f_name = act_name[0]
            l_name = act_name[-1]
            a_names = sql.searchActorNameFull(f_name,l_name)
            return render_template('search.html', movies=a_names, search_type=s_type)
        else:
            a_names = sql.searchActorName(a_name)
            return render_template('search.html', movies=a_names, search_type=s_type)
    elif (s_type == "director"):
        d_name = request.args.get('dir_name','Example')
        print d_name
        if (' ' in d_name):
            di_name = d_name.split(" ")
            f_name = di_name[0]
            l_name = di_name[-1]
            d_names = sql.searchDirectorNameFull(f_name,l_name)
            return render_template('search.html', movies=d_names, search_type=s_type)
        else:
            d_names = sql.searchDirectorName(d_name)
            return render_template('search.html', movies=d_names, search_type=s_type)
    else:
        return render_template('search.html', movies=movies, search_type=None)

@app.route('/', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        # given form username and password
        user_name = request.form.get('userID',None)
        password = request.form.get('password', None)
        # look up user in database
        if user_check(user_name, password) == True:
            # validate password
            # if valid, then log in that user using "login_user(user)"
            user = User(user_name, password)
            login_user(user)
            # redirect
            print 'Logged in successfully'
            return redirect(url_for('create_home'))
        else:
           return render_template('login_error.html') 
    return render_template('login.html')

@app.route('/login_error')
def login_error():
    return render_template('login_error.html')

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

def user_check(user, passw):
    print 'Start check'
    user = str(user)
   
    matches = cursor.execute('SELECT * FROM users WHERE username = "' + user + '"')
    match = cursor.fetchall() 
    if match == None:
        return False
    for person in match:
        if( person[1]==passw):
            return True
    return False 

@login_manager.user_loader
def load_user(userid):
    matches = cursor.execute('SELECT * FROM users WHERE username = "' + userid + '"')
    match = cursor.fetchall() 
    user = User(match[0][0], match[0][1])
    return user

@app.route('/movies/new/', methods=['GET'])
@login_required
def create_movie():
    return render_template('movies/new.html')

#Get a single movie based on ID. 
@app.route('/singlemovie')
@login_required
def movie():
    if request.method =='GET':
        m_id = request.args.get('movie_id')
        # get movie info
        movie = sql.searchMovieID(m_id)
        #Get actors for movie my movie id
        actors = sql.searchMovieActors(m_id)
        #get image
        omdbinfo = ombdreq.getmovie(movie[0][1])
        print omdbinfo
    
        if omdbinfo['Response'] == 'True':
            if omdbinfo['Poster'] == 'N/A':
                omdbimg = '/assets/missingposter.jpg'
            else:
                omdbimg = omdbinfo['Poster']
                
            try:
                omdbrtom = int(omdbinfo['tomatoMeter'])
            except:
                omdbrtom = 'n/a'
        else:
            omdbimg = '/assets/missingposter.jpg'
            omdbrtom = 'n/a'
                    
        #get Genre
        genres = sql.searchMovieGenre(m_id)
        director = sql.searchMovieDirector(m_id)
        print director 
        return render_template('singlemovie.html', movie=movie, actors=actors, img=omdbimg, genres=genres, rtomatoes=omdbrtom, director=director)


@app.route('/home')
@login_required
def create_home():
    return render_template('index.html')

@app.route('/edit_movie', methods=['GET', 'POST'])
@login_required
def edit_movie():
    if request.method == 'GET':
        movie = sql.searchMovieID(request.args.get('movie_id'))
        return render_template('edit_movie.html', movie=movie)
    elif request.method == 'POST':
            movie = str(request.form.get('movie_id'))
            new_title = '\'' + request.form.get('edit_movie') + '\''
            sql.updateTitle(new_title, movie)
            return render_template('alert_succ.html', movie=movie)

@app.route('/add_genre', methods=['GET', 'POST'])
@login_required
def add_genre():
    if request.method == 'GET':
        movie = sql.searchMovieID(request.args.get('movie_id'))
        return render_template('edit_movie.html', movie=movie)
    elif request.method == 'POST':
            movie = str(request.form.get('movie_id'))
            new_genre = '\'' + request.form.get('add_genre') + '\''
            sql.addMovieGenre(new_genre, movie)
            return render_template('alert_succ.html')

@app.route('/remove_genre', methods=['GET', 'POST'])
@login_required
def remove_genre():
    if request.method == 'GET':
        movie = sql.searchMovieID(request.args.get('movie_id'))
        return render_template('edit_movie.html', movie=movie)
    elif request.method == 'POST':
            movie = str(request.form.get('movie_id'))
            genre = '\'' + request.form.get('remove_genre') + '\''
            sql.removeGenre(genre, movie)
            return render_template('alert_succ.html')

@app.route('/delete_movie', methods=['GET', 'POST'])
@login_required
def delete_movie():
    if request.method == 'GET':
        movie = sql.searchMovieID(request.args.get('movie_id'))
        return render_template('delete_movie.html', movie=movie)
    elif request.method == 'POST':
            movie = str(request.form.get('movie_id'))
            sql.removeMovie(movie)
            return render_template('alert_succ.html')

@app.route('/add', methods=['GET','POST'])
@login_required

def add_movie():
    if request.method == 'POST':
        title = request.form.get('movie_title')
        director = request.form.get('dir_name')
        genre = request.form.get('genre_name')
        title = "'" + title + "'"
        m_id = sql.addMovie(title)
        if (director != None):
            if (' ' in director):
                dir_name = director.split(" ")
                d_first = "'" + dir_name[0] + "'"
                d_last = "'" + dir_name[-1] + "'"
                d_id = sql.addDirector(d_first, d_last)
            else:
                director = "'" + director + "'"
                d_id = sql.addDirector(director, None) 
            sql.addMovieDirector( d_id,m_id)
        if (genre != None):
            sql.addMovieGenre(genre, m_id)
        return render_template('add.html')
    return render_template('add.html')

@app.route('/alert_succ')
def alert_succ():
    return render_template('alert_succ.html')


# display an actors pages.
@app.route('/actor')
@login_required
def singleActor():
    actor_id = request.args.get('actor_id')
    actorRoles = sql.searchActorRole(actor_id)
    actor = sql.searchActorName(actor_id)
    return render_template('singleactor.html', roles=actorRoles, actor=actor)

@app.route('/director')
@login_required
def singleDirector():
    director_id = request.args.get('director_id')
    directorMovies = sql.searchDirectorMovies(director_id)
    director = sql.searchDirectorNameID(director_id)
    print directorMovies
    return render_template('singledirector.html', movies=directorMovies, director=director)

@app.errorhandler(404)
def page_not_found(error):
    return render_template('error.html'), 404

if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)
