import requests
import cgi
import re



#
# Sends api request to omdb 
# Return: json string 
#
def getmovie(movie):
    
    # Replaces spaces in string
    movie = clean(movie)
    
    # Sends request to omdb
    url = 'http://omdbapi.com/?t=' + movie + '&tomatoes=True'
    print url
    res = requests.get(url)
    print res.text
    p = re.compile('.*503 Service Temporarily Unavailable.*')
    p2 = re.compile('.*Proxy Error.*')
        
    if (p.match(str(res.text)) is not True) and p2.match(str(res.text)):
        return res.json()
    else:
        return {'Response': 'False'}
#
# Generates http friendly string. All ' ' have been replaced with '+'
# Returns http formated string
#
def clean(dirtyString):
    dirtyString = dirtyString.replace(' ', '+')
    dirtyString = dirtyString.replace('&', '%26')
    cleanString = dirtyString.replace(':', '%3A')
    print cleanString
    return cleanString
